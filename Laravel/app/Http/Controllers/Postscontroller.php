<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return "ID is:".$id;
    }

    public function customer($customerid,$name,$address)
    {
        return "Customer id:".$customerid."<br> Name:".$name."<br> Address:".$address;
    }

    public function item($No,$Name,$price)
    {
        return "No:".$No."<br> Name:".$Name."<br> Address:".$price;
    }
    public function order($customerid,$Name,$OrderNo,$Date)
    {
        return "Customer id:".$customerid."<br> Name:".$Name."<br> OrderNo:".$OrderNo."<br> Date:".$Date;
    }

    public function orderdetails($TransNo,$OrderNo,$Itemid,$Name,$Price,$Qty)
    {
        return "TransNo:".$TransNo."<br> Name:".$Name."<br> OrderNo:".$OrderNo."<br> Price:".$Price."<br> QTY:".$Qty;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
