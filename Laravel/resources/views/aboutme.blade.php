@extends('layouts.app')
@section('content')

<html>
<title>About me</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<body style="background-image: url('hh.png');">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

  <!-- The Grid -->
  <div class="w3-row-padding">
  
    <!-- Left Column -->
    <div class="w3-third">
    
      <div class="w3-white w3-text-grey w3-card-4">
        <div class="w3-display-container">
          <img src="jrgr.jpg" style="width:100%">
          <div class="w3-display-bottomleft w3-container w3-text-black">
            <h2>John Roger Vicente</h2>
          </div>
        </div>
        <div class="w3-container">
          <p><i class="fa-fw w3-margin-right w3-large w3-text-teal"></i>Web Designer</p>
          <p><i class="fa-fw w3-margin-right w3-large w3-text-teal"></i>San Carlos City</p>
          <p><i class=" fa-fw w3-margin-right w3-large w3-text-teal"></i>Ragevicente212@gmail.com</p>
          <p><i class=" fa-fw w3-margin-right w3-large w3-text-teal"></i>0945 986 5696</p>
          <hr>

          <p class="w3-large"><b><i class=" fa-fw w3-margin-right w3-text-teal"></i>Skills</b></p>
          <p>Adobe Photoshop</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-brown" style="width:90%">90%</div>
          </div>
          <p>Photography</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-brown" style="width:80%">
              <div class="w3-center w3-text-white">80%</div>
            </div>
          </div>
          <p>Illustrator</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-brown" style="width:75%">75%</div>
          </div>
          <p>Media</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-brown" style="width:50%">50%</div>
          </div>
          <br>

          <p class="w3-large w3-text-theme"><b><i class=" fa-fw w3-margin-right w3-text-teal"></i>Languages</b></p>
          <p>Filipino</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-red" style="height:24px;width:100%"></div>
          </div>
          <p>English</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-red" style="height:24px;width:55%"></div>
          </div>
          <p>Pangasinan</p>
          <div class="w3-light-grey w3-round-xlarge">
            <div class="w3-round-xlarge w3-red" style="height:24px;width:25%"></div>
          </div>
          <br>
        </div>
      </div><br>

    <!-- End Left Column -->
    </div>
  <!-- Right Column -->

  <div class="w3-twothird">
    
    <div class="w3-container w3-card w3-white w3-margin-bottom">
      <h2 class="w3-text-grey w3-padding-16"><i class=" fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Learnings</h2>
      <div class="w3-container">
        <h5 class="w3-opacity"><b>Laravel Framework</b></h5>
        <h6 class="w3-text-teal"><i class=" fa-fw w3-margin-right"></i></h6>
        <p>Laravel is an open-source PHP framework. It also offers the rich set of functionalities that incorporates the basic features of PHP frameworks
         such as Codelgniter, Yii, and other programming languages like Ruby on Rails.</p>
        <hr>
      </div>
      <div class="w3-container">
        <h5 class="w3-opacity"><b>MVC Structure: An Overview</b></h5>
        <h6 class="w3-text-teal"><i class="fa-fw w3-margin-right"></i></h6>
        <p>The MVC structure means that the application is divided into three parts.
             The (M) model defines the data model, 
            (V) view presents data and (C) controller defines logic and manipulates data.</p>
    
        <hr>
      </div>

      <div class="w3-container">
        <h5 class="w3-opacity"><b>MVC Structure: An Overview</b></h5>
        <h6 class="w3-text-teal"><i class=" fa-fw w3-margin-right"></i></h6>
        <p>The MVC structure means that the application is divided into three parts.
             The (M) model defines the data model, 
            (V) view presents data and (C) controller defines logic and manipulates data.</p>
            <h4 class="w3-opacity">Creating authorization and authentication systems</h4>
        <p>Every owner of the web application makes sure that unauthorized users do not access secured or paid resources. It provides a simple way of implementing authentication. 
            It also provides a simple way of organizing the authorization logic and control
             access to resources.</p>
        
             <h4 class="w3-opacity">Integration with tools</h4>
        <p>Laravel is integrated with many tools that build a faster app. It is not only necessary to build the app but also to create a faster app. Integration with the caching back end is one of the major steps to improve the performance of a web app. 
            Laravel is integrated with some popular cache back ends such as Redis, and Memcached.</p>


             <h4 class="w3-opacity">Mail service integration</h4>
        <p>Laravel is integrated with the Mail Service. This service is used to send notifications to the user's emails. It provides a clean and simple API that allows
             you to send the email quickly through a local or cloud-based service of your choice.</p>

             <h4 class="w3-opacity">Handling exception and configuration error</h4>
        <p>Handling exception and configuration errors are the major factors on the app's usability. The manners in which the software app handles the errors have a huge impact on the user's satisfaction and the app's usability. The organization does not want to lose their customers, so for them, Laravel is the best choice.
             In Laravel, error and exception handling is configured in the new Laravel project.</p>

             <h4 class="w3-opacity">Automation testing work</h4>
        <p>Testing a product is very important to make sure that the software runs without any errors, bugs, and crashes. We know that automation testing is less time-consuming than manual testing,
             so automation testing is preferred over the manual testing. Laravel is developed with testing in mind.</p>

             <h4 class="w3-opacity">Separation of business logic code from presentation code</h4>
        <p>The separation between business logic code and presentation code allows the HTML layout designers to change the look without interacting with the developers. A bug can be resolved by the developers faster if the separation is provided between the business logic code and presentation code.
             We know that Laravel follows the MVC architecture, so separation is already done.</p>

             <h4 class="w3-opacity">Fixing most common technical vulnerabilities</h4>
        <p>The security vulnerability is the most important example in web application development. An American organization, i.e., OWASP Foundation, defines the most important security vulnerabilities such as SQL injection, cross-site request forgery, cross-site scripting, etc. Developers need to consider these vulnerabilities and fix them before delivery. Laravel
             is a secure framework as it protects the web application against all the security vulnerabilities.</p>

             <h4 class="w3-opacity">Scheduling tasks configuration and management</h4>
        <p>The web app requires some task scheduling mechanism to perform the tasks in time for example, when to send out the emails to the subscribers or when to clean up the database tables at the end of the day. To schedule the tasks, developers need first to create the Cron entry for each task,
             but Laravel command scheduler defines a command schedule which requires a single entry on the server.</p>
        
             <h1 class="w3-opacity"><b>Features of Laravel</b></h1>
             <h4 class="w3-opacity">Authentication</h4>
             <h4 class="w3-opacity">Innovative Template Engine</h4>
             <h4 class="w3-opacity">Effective ORM</h4>
             <h4 class="w3-opacity">MVC Architecture Support</h4>
             <h4 class="w3-opacity">Secure Migration System</h4>
             <h4 class="w3-opacity">Unique Unit-testing</h4>
             <h4 class="w3-opacity">Intact Security</h4>
             <h4 class="w3-opacity">Libraries and Modular</h4>
             <h4 class="w3-opacity">Artisan</h4><br><br>
             
             <h1 class="w3-opacity"><b>Setting up a Project</b></h1>
             <h4 class="w3-opacity">composer global require laravel/installer</h4>
        <p>Step 1. We need to install Laravel for our project in the D:\ drive. Open “Terminal” and run the following commands.
        Download the Laravel installer:</p>
        
        <h4 class="w3-opacity">laravel new name of the project</h4>
        <p>Step 2. Next, we can create a new Laravel application in the D:\ directory:</p>

        <h4 class="w3-opacity">laravel new name of the project/folder</h4>
        <p>Step 2. Next, we can create a new Laravel application in the D:\ directory:</p>

        <h4 class="w3-opacity">laravel new Blog</h4>


        <h4 class="w3-opacity">php artisan key:generate</h4>
        <p>Step 3. Start the Server

Before starting the server, it is important to set an application key. Setting an application key is essential to the safety of our project. If the application key is not set, your user sessions and other encrypted data will not be secure.

The following command will set the application key for you:</p>

<h4 class="w3-opacity">php artisan serve</h4>
<p>Finally, it’s time to start our server. We have two choices, we can run the built-in local development server:</p>


             <hr>
      </div>



      <div class="w3-container w3-card w3-white">
        <h2 class="w3-text-grey w3-padding-16"><i class=" fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Education</h2>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Tertiary<br> </br> PSU</b></h5>
          <h6 class="w3-text-teal"><i class=" fa-fw w3-margin-right"></i></span></h6>
          <p>Urdaneta,Pangasinan</p>
          <hr>
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Senior Highschool</br>Vigen Milagrosa University</b></h5>
          
          <p>San Carlos City, Pangasinan</p>
          <hr>
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>Junior Highschool</br>VM Saint Dominic High School</b></h5>
          <p>San Carlos City, Pangasinan</p>
          <hr>
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b>PRE-ELEMENTARY EDUCATION</br>Tarece Integrated SCHOOL</b></h5>
          <p>San Carlos City, Pangasinan</p><br>
        </div>
      </div>
        <div>

        
        </div>
    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
  <!-- End Page Container -->
</div>




</body>
</html>
@endsection