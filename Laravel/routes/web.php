<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Postscontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', [PagesController::class, 'index']);
Route::get('/item/{Name?}/{No?}/{price?}',[PostsController::class, 'item'])->name('itm');



Route::get('/order/{customerid?}/{Name?}/{OderNo?}/{Date?}',[PostsController::class, 'order'])->name('odr');



Route::get('/customer/{customerid?}/{name?}/{address?}',[PostsController::class, 'customer'])->name('cust');


Route::get('/orderdetails/{TransNo?}/{OrderNo?}/{Itemid?}/{Name?}/{Price?}/{Qty?}',[PostsController::class, 'orderdetails'])->name('orderdetail');
   

Route::get('student/details',function()
{
    $url=route('student.details');
    return $url;
})->name('student.details');


Route::get('/post/{id}',[PostsController::class, 'index'])->name('profile');





Route::get('/aboutme', function () {
    return view('aboutme');


});

Route::get('/contactme', function () {
    return view('contactme');


});


Route::resource('/blog', Postscontroller::class);
//Route::resource('/aboutme', Postscontroller::class);
//Route::resource('/contact', Postscontroller::class,);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');
